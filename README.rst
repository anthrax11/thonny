NB!!! The repo is now Git (was Mercurial) !!!
==================================================

!!! Note for paper reviewer !!!
===============================
Current installers provide Thonny version where "Step into" and "Step over" are merged into single command "Step". These installers (0.2.7) are recommended for reviewing main features of Thonny.




Thonny
======

Thonny is a Python IDE meant for learning programming.

Features:

* Statement stepping without breakpoints
* Live variables during debugging
* Stepping through evaluation of the expressions (expressions get replaced by their values)
* Separate windows for executing function calls (helps explaining local variables and call stack; good understanding of how function calls work is especially important for understanding recursion)  
* Variables can be explained either by using simplified model (name -> value) or by using more realistic model (name -> address/id -> value) 

Screenshot:

.. image:: https://bytebucket.org/plas/thonny/raw/822bfda455d57309e84d16b9842c83645ab8eaa6/screenshot.png