Thonny can be launched from command line or via a shortcut. 
It seems that in order to be a default program for a file type,
you need to have an exe file associated with it. This project
creates an exe which executes Thonny's shortcut.