How to add a translation with Poedit (http://poedit.net/)

1. Install and open Poedit
2. Choose File => New from POT/PO file ...
3. Select thonny.pot from this folder
4. Select your language
5. Optionally fill in information about yourself in Catalog => Properties ...
6. Translate
7. Save the file as thonny.po under locale/<your-language-code>/LC_MESSAGES
8. Make sure Poedit also created thonny.mo file

How to update translation when Thonny changes
1. Open your po file
2. Select Catalog => Update from POT file
3. Choose thonny.pot from this folder
4. Translate new messages
5. Save