import sys
from os import path
from re import *

class BrokenClass:

class TestClass:
    variable = 1
    brokevariable = 
    def staticMethod():
        pass
    def instanceMethod(self):
        pass
    def brokenMethod(self):

import                              #<<<<<<<<LINE 1>>>>>>>>

import p                            #<<<<<<<<LINE 2>>>>>>>>

sys.                                #<<<<<<<<LINE 3>>>>>>>>

getfilesys                          #<<<<<<<<LINE 4>>>>>>>>

os.path.                            #<<<<<<<<LINE 5>>>>>>>>

path.                               #<<<<<<<<LINE 6>>>>>>>>

re.                                 #<<<<<<<<LINE 7>>>>>>>>

compi                               #<<<<<<<<LINE 8>>>>>>>>

testob                              #<<<<<<<<LINE 9>>>>>>>>

testobject = TestClass()

testo                               #<<<<<<<<LINE 10>>>>>>>>

testobject.                         #<<<<<<<<LINE 11>>>>>>>>